# coding=UTF-8
import os
import json
import urllib
import urllib2
from urllib import quote, urlopen
from HTMLParser import HTMLParser
import sys
import nltk
#1426450464252318
from const import *
from function import g_f
reload(sys)
sys.setdefaultencoding("utf-8")
'''
	data root is the folder for the result

	interface:
		(catch_group_data) is for a input id, he will get his friend list, and get whole people's joining group, 
		download the group (name, description, feed) to the data folder
'''
data_root = "../data/group_data/"
def get_group_info(id):
	url = "https://graph.facebook.com/" + id + "?access_token=" + access_token
	data = json.loads(urlopen(url).read())
	re_data = {}
	if "description" in data:
		re_data["description"] = data["description"].decode("utf-8","ignore")
	if "name" in data:
		re_data["name"] = data["name"].decode("utf-8","ignore")
	re_text = []

	url = "https://graph.facebook.com/" + id + "/feed?access_token=" + access_token
	data = json.loads(urlopen(url).read())
	if "data" not in data:
		return re_data
	for mes in data["data"]:
		if "message" in mes:
			re_text.append(mes["message"].decode("utf-8","ignore"))
		elif "description" in mes:
			re_text.append(mes["description"].decode("utf-8","ignore"))
		if "comments" in mes:
			for comment in mes["comments"]:
				if "message" in comment:
					re_text.append(comment["message"].decode("utf-8","ignore"))
	re_data["feed"] = re_text
	return re_data

def catch_group_data(root_id):
	friends = g_f(root_id) # get friend
	friends_id = [friend[0] for friend in friends]
	friends_id.append(root_id)
	groupid = {}
	for id in friends_id:
		print "id:" + id + "####################"
		if str(id) == root_id:
			url = "https://graph.facebook.com/me/groups?access_token=" + access_token
		else:
			url = "https://graph.facebook.com/" + id + "/groups?access_token=" + access_token
		data = json.loads(urlopen(url).read())	
		if "data" not in data:
			continue
		for group in data["data"]:
			groupid.setdefault(group["id"], [])
			groupid[group["id"]].append(id)

	for id in groupid.keys():
		print "get:" + id
		data = get_group_info(id)		
		data["member"] = groupid[id]
		f = open(data_root + id, "w")
		f.write(json.dumps(data))
		f.close()

#check_data is usually of no use.....
def check_data():
	for file in os.listdir(data_root):
		if os.path.isdir(file):
			continue
		if file.startswith("."):
			continue
		f = json.loads(open(os.path.join(data_root,file), "r").read())
		print file, len(f["member"]), f["name"]

if __name__ == "__main__":
	pass
