import json
data_root = "../data/david_friend_output/"
'''
this is just used to get the node link group data to one neo4j standard format(details can be seen
	in the sendtoneo4j.py
	)

it's easy to see, some number needs to be changed d3_0_300 represents the number in friend list from 0 - 300.
it's can be changed according to your need.
'''
if __name__ == "__main__":
	d1 = json.loads(open(data_root + "d3_0_300.json").read())
	d2 = json.loads(open(data_root + "d3_301_600.json").read())
	d3 = json.loads(open(data_root + "d3_601_1140.json").read())
	d = {}
	d["node"] = []
	d["node"] += d1["node"]
	d["node"] += d2["node"]
	d["node"] += d3["node"]
	d["link"] = json.loads(open(data_root + "link.json").read())
	d["group"] = json.loads(open(data_root + "group.json").read())
	f = open(data_root + "neo4j_data.json", "w")
	f.write(json.dumps(d))
	f.close()