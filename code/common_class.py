# coding=UTF-8
import sys
from HTMLParser import HTMLParser
from nltk.corpus import conll2000
import nltk
import urllib
reload(sys)
sys.setdefaultencoding("utf-8")
'''
	this file include:
		class: (my_parse) (a html parse for interpreting the feed from a person's homepage)
		class: (conll_chunker) (for language processing, get chunker(just NP chunker) from a setence,
			if you want to change chunker, you can change code:
			more detail: http://www.nltk.org/book/ch07.html
		)
'''
class my_parse(HTMLParser):
	def __init__(self):
		HTMLParser.__init__(self)
		self.datas = []
		self.judge_user = 0
		self.judge_share = 0
		self.depth = 0
		self.str = ""
		self.begin = ""
		
	def handle_starttag(self, tag, attrs):
		self.judge_share = 0
		if self.judge_user > 0:
			self.judge_user += 1
		for name,value in attrs:
			#because of the post are belong to userContent class
			#shared content belong to dir = "ltr"
			#so according to these two features to catch data
			if name == "class" and value.find("userContent") != -1 and value.find("userContentSecondary") == -1:
				self.judge_user = 1
				break
			if name == "dir" and value == "ltr":
				self.judge_share = 1
				break

	def handle_endtag(self, tag):
		if self.judge_user > 0:
			self.judge_user -= 1
			if self.judge_user == 0:
				self.datas.append(self.str)
				self.str = ""

	def handle_data(self,data):
		if self.judge_share:
			self.datas.append(data)
		elif self.judge_user:
			self.str += " " + data

class conll_chunker(nltk.ChunkParserI):
	def __init__(self, train_sents=conll2000.chunked_sents('train.txt', chunk_types=['NP'])):
		train_data = [[(t,c) for w,t,c in nltk.chunk.tree2conlltags(sent)] for sent in train_sents]
		self.tagger = nltk.TrigramTagger(train_data)
		#use a trigram as a feature for training
	def parse(self, sentence):
		pos_tags = [pos for word, pos in sentence]
		tagged_pos_tags = self.tagger.tag(pos_tags)
		conlltag = []
		for i in range(len(sentence)):
			conlltag.append((sentence[i][0], sentence[i][1], tagged_pos_tags[i][1]))
		return conlltag

	def parse_feature(self,sentence):
		con_list = self.parse(sentence)
		str = ""
		arr_str = []
		flag = False
		ok_flag = False
		for word, pos, tag in con_list:
			#NP chunker
			if tag is not None and tag.find("NP") != -1:
				if flag:
					ok_flag = True
				flag = True
				str += word
			else:
				if str != "":
					if ok_flag:
						arr_str.append(str)
					str = ""
				flag = False
				ok_flag = True
		if str != "" and ok_flag:
			arr_str.append(str)
		return arr_str

if __name__ == "__main__":
	pass
	
