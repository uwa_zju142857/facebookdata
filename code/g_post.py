# coding=UTF-8
import os
import json
import urllib
import urllib2
from urllib import quote
from HTMLParser import HTMLParser
from function import g_f
from const import *
import sys
import nltk
h = HTMLParser()
reload(sys)
sys.setdefaultencoding("utf-8")
from common_class import my_parse
'''  
	this file is used to get the friend post:
	usage:
		g_f_p(id) is to get a person's friend post given id
		g_p(id) is to get a person's post give id
'''

data_root = "../data/david_friend_post/"
def g_p(id):
	global h
	id = str(id)
	parser = my_parse()
	person = json.loads(urllib.urlopen(url = "http://graph.facebook.com/" + str(id)).read())
	#if "link" not in person:
	#	print id,person["first_name"], person["last_name"],"no link"
	#	return None
	print id
	url = "https://www.facebook.com/profile.php?id=" + id + "&frefc=pb&hc_location=friends_tab"
	req = urllib2.Request(url)
	browser='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36'
 	req.add_header('User-Agent',browser)
 	req.add_header('accept','text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8')
 	req.add_header('cookie',cookie)
 	res = urllib2.urlopen(req).read()
	f = open("1.html","w")
 	f.write(res)
 	f.close()
 	res = res.replace('<!--',' ')
 	res = res.replace('-->',' ')
 	res = h.unescape(res)
 	parser.feed(res)
	return parser.datas

def g_f_p(id):
	arr = g_f(id)
	for item in arr:
		arr_text = g_p(item[0])
		if arr_text is None:
			continue
		f = open(data_root + item[0], "w")
		f.write('.'.join(arr_text))
		f.close()

if __name__ == '__main__':
	#id = 219500287
	#id = 100000195307838
	#print g_p(id)
	#g_f_p(id)

