# coding=UTF-8
import os
import json
import urllib
import urllib2
from urllib import quote
from HTMLParser import HTMLParser
import sys
from function import single_lan_detect
from const import *
reload(sys)
sys.setdefaultencoding("utf-8")
from common_class import my_parse

'''
	no interface
	set the (ROOT_ID)
	set (data_root) which is the friend list to be single_lan_detect
	set (out_root) which is the id with language detected json file stored
	usage:
		python g_lan.py l r
		
		l r represents the range in the friend list.
		like you have 1200 friends
		just python g_lan.py 0 400 python g_lan.py 401 800
			python g_lan.py 801 1200 for three processs
			for the result, just run settle_neo4j_data.py to get the (language file link file and group file) to one
			(neo4j_data.json) to send to neo4j to visualize.

	you can change the code to multi_thread....I just use multi_process to accelerate, because I don't get 
	accurate result using multi_thread...
'''

data_root = "../data/david_friend/"
out_root = "../data/david_friend_output/"
ROOT_ID = "......"

def g_l(id):
	id = str(id)
	parser = my_parse()
	if id == ROOT_ID:
		url = "https://graph.facebook.com/me?access_token=" + access_token
	else:
		url = "https://graph.facebook.com/" + str(id) + "?access_token=" + access_token
		
	json_str = urllib.urlopen(url).read()
	person = json.loads(json_str)
	if "link" not in person:
		link = "https://www.facebook.com/" + str(id)
	else:
		link = person["link"]
	req = urllib2.Request(link)
	browser='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36'
 	req.add_header('User-Agent',browser)
 	req.add_header('accept','text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8')
 	req.add_header('cookie', cookie)
 	res = urllib2.urlopen(req).read()
 	res = res.replace('<!--',' ')
 	res = res.replace('-->',' ')
 	res = res.decode("UTF-8")
 	parser.feed(res)
	return '.'.join(parser.datas)

node_st = set()
name = {}
link = []

for file in os.listdir(data_root):
	if os.path.isdir(file) or file.startswith("."):
		pass
	else:
		f = open(os.path.join(data_root, file), "r")
		data = json.loads(f.read())
		for key in data:
			node_st.add(key)
			for to in data[key]:
				node_st.add(to[0])
				name[to[0]] = to[1] + " " + to[2]
				link.append({
						"source": key,
						"target": to[0],
						"weight": 1
					});

		f.close()

data = link
f = open(out_root + "link.json","w")
f.write(json.dumps(data))
f.close()

if len(sys.argv) != 3:
	print "wrong parameter size"
	print "usage: python g_lan.py l r"
	sys.exit()

l = int(sys.argv[1])
r = int(sys.argv[2])

try:
	print l, r
	node = []
	cnt = 0
	print len(node_st)
	for id in node_st:
		if cnt >= l and cnt <= r:
			text = g_l(id)
			if text is not None:
				language = single_lan_detect(text.encode('utf8'))
			else:
				language = "und"
			node.append((id,language, name[id]))
			print id, cnt, language
		cnt += 1
finally:
	data = {}
	data["node"] = node
	f = open(out_root + "d3_" + str(l) + "_" + str(r) + ".json","w")
	f.write(json.dumps(data))
	f.close()



