# coding=UTF-8
import urllib
from urllib import urlopen
from urllib import urlencode
from urllib import quote
import urllib2
import json
import re
import base64
from nltk import word_tokenize
from const import * 
#some common function
'''
interface for you to use:
	(translate) is for translate language from src to des
	(g_f) is for get the friend list given a person id PS: we need crawl because some people's friend list isn't complete from develop api
	(save) is using g_f to get the graph connection of somebody
	(single_lan_detect) is using google api to detect the language given the text
	
'''

data_root = "../data/"
def getvalue(str):
	ret = 0
	for cr in str:
		ret = ret * 256 + ord(cr)
	return ret

def index(str, list):
	for i in range(len(str)):
		if str[i] in list:
			return i
	return -1

def erase(string):
	res = ""
	length = len(string)
	idx = 0
	while idx < length:
		val = ord(string[idx])
		shift = 0
		bestval = 128
		while val & bestval:
			shift += 1
			bestval >>= 1
		if shift == 0:
			res += string[idx]
			shift = 1
		idx = idx + shift
	return res

def translate(str, src, des):
	url = "http://translate.google.cn/translate_a/t?client=t"
	url += "&text=" + quote(str)
	url += "&hl=zh-CN"
	url += "&sl=" + src
	url += "&tl=" + des
	url += "&ue=UTF-8&oe=UTF-8"
 	req = urllib2.Request(url)
 	browser='Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)'
 	req.add_header('User-Agent',browser)
 	response = urllib2.urlopen(req)
 	html = response.read()
 	index1 = html.find("\"")
 	index2 = html[index1 + 1:].find("\"")  + index1 + 1
 	return html[index1 + 1:index2]

#loop to get all friends of a person
def loop_g(id, tid):
	global cookie
	id = str(id)
	tid = str(tid)
	url = "https://www.facebook.com/ajax/pagelet/generic.php/AllFriendsAppCollectionPagelet?data=%7B%22collection_token%22%3A%22" + id + "%3A2356318349%3A2%22%2C%22cursor%22%3A%22" + quote(base64.encodestring("0:not_structured:" + tid)[:-1]) + "%22%2C%22tab_key%22%3A%22friends%22%2C%22profile_id%22%3A" + id + "%2C%22overview%22%3Afalse%2C%22ftid%22%3Anull%2C%22order%22%3Anull%2C%22sk%22%3A%22friends%22%2C%22importer_state%22%3Anull%7D&__user=" + id + "&__a=1&__rev=1346601"
	req = urllib2.Request(url)
	browser='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36'
 	req.add_header('User-Agent',browser)
 	req.add_header('accept','text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8')
 	#cookie needed
 	req.add_header('cookie',cookie)
 	html = urllib2.urlopen(req).read()
 	idx = 0
	cnt = 0
	arr = []
	while True:
		idx = html.find("eng_tid")
		if idx == -1:
			break
		cnt += 1
		html = html[idx + 20:]
		fid = int(html[0:html.find("&")])
		tid = fid
 		pr = get_person(fid)
 		if "id" in pr and "first_name" in pr and "last_name" in pr:
			arr.append((pr["id"], pr["first_name"], pr["last_name"]))
 	if cnt != 20:
 		return arr
 	else:
 		arr += loop_g(id, tid)
 		return arr
 		
# get the first 20, and use the last id to construct url to get persons left
def root_g(id):
	global cookie
	url = "https://www.facebook.com/profile.php?id=" + str(id) + "&sk=friends"
	req = urllib2.Request(url)
	browser='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36'
 	req.add_header('User-Agent',browser)
 	req.add_header('accept','text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8')
 	#cookie needed
 	req.add_header('cookie',cookie)
 	res = urllib2.urlopen(req).read()
 	p = re.compile(r"fsl fwb fcb.*?data-gt.*?</a>")
 	f = open("1.html","w")
 	f.write(res)
 	f.close()
 	r1 = re.findall(p, res)
 	res = 0
 	arr = []
 	for string in r1:
 		idx = string.find("eng_tid")
 		string = string[idx + 20:]
 		id = int(string[0:string.find("&")])
 		pr = get_person(id)
 		if "id" in pr and "first_name" in pr and "last_name" in pr:
 			arr.append((pr["id"], pr["first_name"], pr["last_name"]))
 			res = pr["id"]
 	return (res, arr)

def g_f(id):
	id = str(id)
	print "#############################you are asking: " + id
	pr = get_person(id)
	tid, arr = root_g(id)
	arr += loop_g(id, tid)
	return arr


def get_person(id):
	person = json.loads(urlopen(url = "http://graph.facebook.com/" + str(id)).read())
	return person

def save(id):
	id = str(id)
	arr = g_f(id)
	data = {}
	data[id] = arr
	f = open(data_root + "david_friend/" + id + ".txt", "w")
	f.write(json.dumps(data))
	f.close
	for idx in arr:
		print idx
		data = {}
		data[idx[0]] = g_f(idx[0])
		f = open(data_root + "david_friend/" + idx[0] + ".txt", "w")
		f.write(json.dumps(data))
		f.close

def single_lan_detect(text):
	url = "https://www.googleapis.com/language/translate/v2/detect?key=" + google_detect_key + "&q=" + quote(text)
	#print url
	try:
		language = json.loads(urllib.urlopen(url).read()) #["data"]["detections"][0][0]["language"]
		return language
	except:
		return None

def judge_en(sentence):
	for char in sentence:
		if ord(char) > 127:
			return False
	return True

if __name__ == '__main__':
	pass

