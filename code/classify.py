# coding=UTF-8
import nltk
from nltk.corpus import brown
from nltk.corpus import wordnet as wn
import Queue
from nltk import word_tokenize
from nltk import pos_tag
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from nltk.stem.snowball import SnowballStemmer
import pickle
from sklearn.externals import joblib
from sklearn.lda import LDA
import json
import sys
import os
import math
from function import judge_en
from common_class import conll_chunker
from nltk.corpus import stopwords
from function import single_lan_detect
from language_divider import guess
reload(sys)

sys.setdefaultencoding("utf-8")

MAX_SENT = 0
TF_IDF_RATE = 0.025
MERGE_PER = 0

'''
	this class mainly contain a class (TopicDivide)
	
	usage:
		python classify.py (train | test | test_dir) (bayes | lda) MAX_SENT MERGE_PER

		another usage hasn't been listed in the argv, you need to change the code:
		
		line 129 : def word_to_vt(self, arr_word, correct=False):
			you can change default parameter correct to be True
			thus every time you get a vector, if the word doesn't appear in the corpus,		
			we will find the 1-edit-distance word to replace, like hallo, we may find hello to replace.
		
		the usage mainly 

		parameter:
			train: include train the data which is mainly stored in (data_root)
			and model that's trained stores in (model_root)

			test: you input a setence, it will show the result

			test_dir: get the group data stored to test and result is stored in group.json

			bayes and lda are two different kinds of classifier, we may find bayes is much better.

			MAX_SENT is the setence we choose to train for each category, because the limit of memory and time cost, we 
			need set MAX_SENT

			MERGE_PER is the train unit, like MERGE_PER = 10, we may conbine 10 setences to be a vector to insert to the
			training set.
'''

#frequency chosing
#FREQ_BOUND = 10

#lda_topic_1000.pkl 
#addding porter stemming and distance correction

#add chunk and tf-idf filter

model_root = "../model/"
data_root  = "../data/topic_data/"


#check if contain letter
def containletter(sent):
	for char in sent:
		if ord(char) >= ord('A') and ord(char) <= ord('Z'):
			return True
		if ord(char) >= ord('a') and ord(char) <= ord('z'):
			return True
	return False

class TopicDivide:
	#technology: domain: NATURAL_SCIENCE and APPLIED_SCIENCE and SOCIAL_SCIENCE
	#politics: categories(): government 
	#sports: domain: SPORT
	def __init__(self):
		#init all topics
		self.topics = ["business_fb","education_fb","environment_fb","family_fb","food_fb","friend_fb","health_fb","entertainment_fb","politics_fb", "sport_fb", "tech_fb"]
		self.stemmer = SnowballStemmer("english")
		self.chunker = conll_chunker()

	def load(self, string):
		#load different models according string given
		if string == "lda":
			self.model = joblib.load(model_root + "lda/lda_topic_" + str(MAX_SENT) + ".pkl")
		else:
			self.model = joblib.load(model_root + "bayes/bayes_topic_" + str(MAX_SENT) + ".pkl")
		self.idx_word = json.loads(open(model_root + "topic_word_" + str(MAX_SENT) + ".dat","r").read())

		self.word_count = len(self.idx_word.keys())
		print self.word_count

	def getvol(self, noun):
		#use wordnet to give seeds from a noun given
		q = Queue.Queue()
		syns = wn.synsets(noun)
		words = set()
		for syn in syns:
			q.put(syn)
		while not q.empty():
			syn = q.get()
			for word in syn.lemma_names:
				words.add(word)
			for child_syn in syn.hyponyms():
				q.put(child_syn)
		return words

	#given a setence get the inside word token
	def getwordtoken(self,sent):

		word_token = pos_tag(word_tokenize(sent))
		word_tag_token = [(self.stemmer.stem(word.lower().decode("utf-8","replace")), tag) for word, tag in word_token]
		np_chunker_arr_word = self.chunker.parse_feature(word_tag_token)
		arr_word = [word for word, tag in word_tag_token if containletter(tag)]
		#filter some ! : ( and )
		arr_word += np_chunker_arr_word
		arr_word = [word for word in arr_word if word not in stopwords.words('english')]
		return arr_word		
	#index the word and change the word to vector, edit_distance is calculated is given if correct is True
	def word_to_vt(self, arr_word, correct=False):
		vt = [0.0] * self.word_count
		for word in arr_word:
			if word in self.idx_word:
				vt[self.idx_word[word]] += 1
			else:
				if correct:
					for key in self.idx_word.keys():
						#cal edit distance to correct the word
						if nltk.metrics.edit_distance(key, word) <= 1:
							vt[self.idx_word[key]] += 1
							break

		# normalize
		sum = 0.
		for item in vt:
			sum += item * item
		
		if sum == 0:
			return vt,False #no information provided to train.

		for i in range(len(vt)):
			vt[i] /= math.sqrt(sum)

		return vt,True

	# get feature of a setence to a vector
	def getfeature(self, sentence):

		print "begin getwordtoken"
		arr_word = self.getwordtoken(sentence)
		print arr_word
		print "getwordtoken done"
		print "begin word to vt"
		return self.word_to_vt(arr_word, correct=False)
		print "all done"

	# use tfidf filter some useless words with typical words left.
	def tfidf_filter(self):

		a = []
		l = len(self.topics)
		for i in range(self.word_count):
			a.append([[0] * (len(self.topics) + 1), i])
		for i in range(l):
			for j in range(self.word_count):
				val = 1.0 * self.word_hash[self.word_list[j]][i] / self.topic_word_count[i]
				a[j][0][i] = val
		for j in range(self.word_count):
			aver = 0.
			maxn = 0.
			for i in range(l):
				aver += a[j][0][i]
				if a[j][0][i] > maxn:
					maxn = a[j][0][i]
			aver /= l
			a[j][0][l] = maxn * math.log(0.1 + maxn / aver)

		a = sorted(a, key = lambda x:-x[0][l]) 

		self.word_count = int(self.word_count * TF_IDF_RATE)
		
		#re index
		self.idx_word = {}
		
		for i in range(self.word_count):
			self.idx_word[self.word_list[a[i][1]]] = i
		a = a[:self.word_count]

		f = open(model_root + "tmpdata_" + str(MAX_SENT) + ".log","w") #log data
		for i in range(self.word_count):
			f.write(str(a[i][1]) + " " + str(self.word_list[a[i][1]]) + " " + str(a[i][0][l]))
			f.write("\n")
		f.close()

	#index the word
	def ind_word(self):
		self.idx_word = {}
		self.word_list = []
		self.sents = []
		self.word_count = 0
		self.word_hash = {}
		self.topic_word_count = [0] * len(self.topics)
		cnt = 0
		for idx, topic in enumerate(self.topics):
			f = open(data_root + topic + "/data", "r")
			max_cnt = MAX_SENT
			while True:
				if max_cnt <= 0:
					break
				sent  = f.readline()
				if not sent:
					break
				if sent.startswith("CURRENT URL") or (not judge_en(sent)):
					continue
				print idx,max_cnt
				max_cnt -= 1
				if max_cnt <= 0:
					break
				#add stemmer and chunker and lower
				arr_word = self.getwordtoken(sent)
				self.sents.append((arr_word, idx))
				self.topic_word_count[idx] += len(arr_word)
				for word in arr_word:
					self.word_hash.setdefault(word, [0] * len(self.topics))
					self.word_hash[word][idx] += 1
					if word not in self.idx_word:
						self.idx_word[word] = cnt
						cnt += 1
						self.word_list.append(word)
			f.close()
		self.word_count = cnt
		print self.word_count
		self.tfidf_filter()

	#training
	def get_raw_data(self):

		data = []
		target = []
		print "begin get vector"
		#compress setence and save memory
		per = MERGE_PER
		arr = []
		last_i = 0
		i = 0
		print len(self.sents), self.word_count
		for sent in self.sents:
			if sent[1] != last_i:
				vt,res = self.word_to_vt(arr)
				if res:
					print last_i
					data.append(vt)
					target.append(last_i)
				arr = sent[0]
				i = 1
			else:
				i += 1
				arr += sent[0]
				if i % per == 0:
					i = 0
					vt,res = self.word_to_vt(arr)
					if res:
						print sent[1]
						data.append(vt)
						target.append(sent[1])
			last_i = sent[1]
		vt,res = self.word_to_vt(arr)
		if res:
			data.append(vt)
			target.append(last_i)
		return data,target

	#use different classifier to accept training vector 
	def lda_train(self):
		print "begin lda train"
		self.lda = LDA(n_components=200)
		self.ind_word()
		data, target = self.get_raw_data()
		print "begin lda fit"
		self.lda.fit(data, target)
		print "lda train done"
		joblib.dump(self.lda, model_root + "lda/lda_topic_" + str(MAX_SENT) + ".pkl")
		f = open(model_root + "topic_word_" + str(MAX_SENT) + ".dat", "w")
		f.write(json.dumps(self.idx_word,ensure_ascii=False))
		f.close()

	def bayes_train(self):

		print "begin bayes train"
		trainset = []
		self.mnb = MultinomialNB()
		self.ind_word()
		data, target = self.get_raw_data()
		print "begin bayes fit"
		self.mnb.fit(data, target)
		print "bayes train done"
		joblib.dump(self.mnb,model_root + "bayes/bayes_topic_" + str(MAX_SENT) + ".pkl")
		f = open(model_root + "topic_word_" + str(MAX_SENT) + ".dat", "w")
		f.write(json.dumps(self.idx_word,ensure_ascii=False))
		f.close()

	def test(self):
		while True:
			a = raw_input().strip()
			label = self.model.predict(self.getfeature(a)[0])
			print self.model.predict_proba(self.getfeature(a)[0])
			print label[0], self.topics[label[0]]
				
	# test group data to group.json to be used to settle to a neo4j data
	def test_group(self):
		group_root = "../data/david_group_data"
		out_root = "../data/david_friend_output/"
		group_data = []
		for file in os.listdir(group_root):
			if os.path.isdir(file) or file.startswith("."):
				pass
			else:
				data = json.loads(open(os.path.join(group_root, file), "r").read())
				print file
				str = data["name"]
				if "description" in data:
					str += "." + data["description"]
				str = str.strip().replace('\n','')
				if judge_en(str):
					data.pop("feed")
					data["belong"] = self.topics[self.model.predict(self.getfeature(str)[0])][:-3]
					data["id"] = file
					group_data.append(data)

		f = open(out_root + "group.json", "w")
		f.write(json.dumps(group_data))
		f.close()

	def test_dir(self, dir):
		for file in os.listdir(dir):
			if os.path.isdir(file):
				pass
			else:
				f = open(os.path.join(dir, file), "r")
				context = f.read()
				f.close()
				res = self.test_sent(context)


if __name__ == "__main__":
	test = TopicDivide()
	#test.ind_word()
	#test.bayes_train()
	#test.lda_train()
	if len(sys.argv) != 5:
		print "ussage: python classify train (lda|bayes) max_sent merge_sent,"
		print "python classify test (lda|bayes) max_sent merge_sent,"
		print "python classify test_dir (lda|bayes) max_sent merge_sent"
		sys.exit(0)

	MAX_SENT = int(sys.argv[3]) 
	MERGE_PER = int(sys.argv[4])
	if sys.argv[1] == "train":

		if sys.argv[2] == "lda":
			test.lda_train()
		elif sys.argv[2] == "bayes":
			test.bayes_train()
		else:
			print "wrong paramter, use lda or bayes"
			sys.exit(0)

	elif sys.argv[1] == "test":
		print "begin test:"
		if sys.argv[2] == "lda":
			test.load("lda")
			test.test()
		elif sys.argv[2] == "bayes":
			test.load("bayes")
			test.test()
		else:
			print "wrong paramter, use lda or bayes"
			sys.exit(0)
	elif sys.argv[1] == "test_dir":
		print "begin test dir:"
		if sys.argv[2] == "lda":
			test.load("lda")
			test.test_group()
		elif sys.argv[2] == "bayes":
			test.load("bayes")
			test.test_group()
		else:
			print "wrong paramter, use lda or bayes"
			sys.exit(0)
	else:
		print "ussage: python classify train (lda|bayes) max_sent merge_sent or"
		print "python classify test (lda|bayes) max_sent merge_sent"
		sys.exit(0)
		


#test.lda_train()
#test.ac("bayes")
#test.test_dir("friend_post")
#test.test_dir("david_friend_post")
'''

while True:
	a = raw_input().strip()
	print test.test_sent(a)

sport
basketball
pushball
baseball
volleyball
AFL
softball
football
soccer

biotechnology
IT
computing
robotics
engineering
technology
AI
MT
artificial_intelligence
computer_science
electrical_engineering
EE

Personal Fitness
Business
Family

nltk chunking

设置域值 tf-idf 进行筛选

'''


