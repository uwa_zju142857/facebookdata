# coding=UTF-8
from function import *
import nltk
from nltk import word_tokenize
from nltk import sent_tokenize


''' 
	this file contain two import classes for language divide
		class: (ngram) uses ngram to construct corpus of each category
			and then use funcion (eva) to get the cosine value of the setence and each category corpus.

		class: (guess) is aimed to divide the language by function (process)
			originally we need to use a ngram to train the data, and use function (eva) to detect language,
			and then combine this with a method called sliding window algorithm to divide language...
			which is now implied in function (deal and en_deal), (deal) just uses utf-8 coding format to divide.
			but for different language like French and English, we need sliding window algorithm to divide which is implied
			in (en_deal).
			and for the language detection, originally we use ngram to judge, but because lack of corpus, we use
			google api to detect. and sliding window and utf-8 coding to divide.

	apart from these two cases:
	
		function: (multi_lan_proportion) use the class guess to judge setence, and return the proportion of each language used
				  (multi_lan_devide) use the class guess too, but return each part and corresponding language used

'''

class ngram:
	#bigram is used to get the divide place according to the paper
	#however because lack of traning data, we replace by confidence by google api
	bigram_word = set()
	def __init__(self, language, number):
		f = open(language + ".txt")
		pair = ' ' * number
		self.d = {}
		self.number = number
		for line in f:
			for letter in line.strip():
				pair = pair[1:-1] + pair[-1] + letter
				self.d.setdefault(pair, 0)
				self.d[pair] += 1
			arr_word = line.strip().split(' ')
			for i in range(0,len(arr_word) - 1):
				ngram.bigram_word.add("#".join(arr_word[i:i + 2]))
		self.length = 0
		for x in self.d.values():
			self.length += x * x
		self.length **= 0.5
		
	def eva(self, setence):
		pair = '   '
		d1 = self.d
		length1 = self.length
		d2 = {}
		pair = ' ' * self.number
		for letter in setence.strip():
			pair = pair[1:-1] + pair[-1] + letter
			d2.setdefault(pair, 0)
			d2[pair] += 1
		length2 = 0
		for x in d2.values():
			length2 += x * x
		length2 **= 0.5

		dot = 0
		for item in d1.keys():
			if item in d2:
				dot += d1[item] * d2[item]

		return float(dot) / (length1 * length2)

class guess:
	lan = ["English", "Danish", "French", "Finnish", "Swedish"]
	ng =  [[ngram("../data/language/" + language, i) for language in lan] for i in range(1,6)]
	
	#prior given by the paper

	prior = [[0], [1,0], [1,2,0], [2,1,3,0], [2,3,1,4,0]]
	
	def eva(self, setence, n=None):
		maxscore = -1
		if n is not None:
			ran = range(n, n + 1)
		else:
			ran = range(0, 5)
		idx = -1
		for i in range(0,5):
			score = 0
			for j in ran:
				score += (j + 1) * guess.ng[j][i].eva(setence)
			if maxscore == -1 or score > maxscore:
				maxscore = score
				idx = i
		return idx

	def process(self, str):
		str = str.lower()
		sents = sent_tokenize(str)
		filter = ["http", "@", "#"]
		#filter some social element
		res = []
 		for sent in sents:
 			flag = True
 			for fil in filter:
 				if sent.startswith(fil):
 					flag = False
 					break
 			if flag:
 				res += self.deal(sent, 4)
 		return res
	def deal(self, str, n):
		res = []
		l = len(str)
		i = 0
		last_lan = None
		last_idx = 0
		while i < l:
			#space is common for all language
			if str[i] in [',','.',' ']:
				i = i + 1
				continue
			biggest = 128
			nxtlen = 0
			while ord(str[i]) & biggest:
				nxtlen = nxtlen + 1
				biggest >>= 1
			
			if nxtlen == 0:
				nxtlen = 1

			val = getvalue(str[i:i + nxtlen])
			#first judge language
			if nxtlen == 1:
				if val >= ord('0') and val <= ord('9'):
					lan = "Digit"
				else:
					lan = "latin"
			elif nxtlen == 2:
				if val > 0xc280 and val < 0xc9bf:
					lan = "latin"
				if val > 0xd880 and val < 0xdc80:
					lan = "Arabic"	
			elif nxtlen == 3:
				if val >= 0xE4B880 and val <= 0xe9bfbf:
					lan = "Chinese"
				elif val >= 0xe38080 and val <= 0xe383bf:
					lan = "Japanese"
				elif (val >= 0xe18480 and val <= 0xe187bf) or (val >= 0xe384b0 and val <= 0xe3868f) or (val >= 0xeab080 and val <= 0xed9eaf):
					lan = "Korean"
				else:
					lan = last_lan
			else:
				lan = "latin"

			if last_lan is not None and last_lan != lan:
				#the second step to judge latin
				if(last_lan == "latin"):
					res += self.en_deal(str[last_idx:i], n)
				else:
					res.append((last_lan, str[last_idx:i]))
				last_idx = i

			i += nxtlen
			last_lan = lan

		if last_lan == "latin":
			res += self.en_deal(str[last_idx:l], n)
		else:
			res.append((last_lan, str[last_idx:l]))

		return res
			

	def en_deal(self, setence, n):
		res = []
		arr_word = setence.strip().split()
		idx = 0
		last_lan = None
		length = len(arr_word)
		break_where = []
		last_divide = -1
		while idx + n <= length:
			#because less of train data, so use google api for detect
			#slide window algorithm is used
			lan = single_lan_detect(' '.join(arr_word[idx:idx + n]))["data"]["detections"][0][0]["language"]
			if last_lan is not None and lan != last_lan:
				minn = -1
				#use google api for 
				break_pos = -1
				for j in range(idx, idx + n - 1):
					tres = single_lan_detect(' '.join(arr_word[j:j + 2]))
					#use google confidence for break
					val = float(tres["data"]["detections"][0][0]["confidence"])
					if minn == -1 or val < minn:
						val = minn
						break_pos = j
				idx = break_pos + 1
				res.append((last_lan, ' '.join(arr_word[last_divide + 1:idx])))
				last_divide = idx - 1
			else:
				idx = idx + 1
			last_lan = lan
		if last_lan is None:
			last_lan = single_lan_detect(' '.join(arr_word[last_divide + 1:length]))["data"]["detections"][0][0]["language"]
		res.append((last_lan, ' '.join(arr_word[last_divide + 1:length])))
		return res



def multi_lan_devide(text, judge = guess()):
	return judge.process(text)	

def multi_lan_proportion(text, judge = guess()):
	hash = {}
	total = 0
	res = judge.process(text)
	print res
	for lan, arr_word in res:
		if lan == "Digit" or lan is None:
			continue
		hash.setdefault(lan, 0)
		hash[lan] += 1
		total += 1
	res = []
	for key in hash.keys():
		if key == "Digit" or key is None:
			continue
		res.append((key, 1.0 * hash[key] / total))

	return sorted(res, key = lambda x:x[1])

if __name__ == '__main__':
	res =  multi_lan_devide("Merci, merci mes amis, pour vos bons voeux! Thanks, my dear friends, for your wishes!")
	print res
