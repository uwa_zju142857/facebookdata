import json
import urllib2
import os
import sys
root = "http://localhost:7474/db/data/transaction/commit"
nodelabel = "facebooknode"
node_st = {}
data_root = "../data/"
'''
	this is the file to clear the database and 
	then send the data stored in a json file to the neo4j database using http.

	make sure the neo4j has start:
		cd neo4j-community-2.1.3/
		bin/neo4j start

	the file format
	is a json.dumps(hash)

	hash has three attributes:
	1.node
		each node is a list
		(id, language, name)
	2.link
		each link has
		2.1 source (node id)
		2.2 target (node id)
	3.group
		each group has
		3.1 id
		3.2 name
		3.3 belong
		3.3 member (a array of node id)

'''

'''
neo4j search usage:

1.search relation: 

	match (a:_facebook) - [r] -> (b:_facebook) return r

2.search node:

	match (a:_facebook) return a

3.triangle connection:

	match (a:_facebook) - [r1] -> (b:_facebook) match (b:_facebook) - [r2] -> (c:_facebook) match (c:_facebook) - [r3] -> (a:_facebook) return r1, r2, r3 limit 1000

4.find a person's relation: 

	match (a:_facebook {id:'100005519621836'}) - [r:friend] -> (b:_facebook) return r
	
5. find two persons' relation:

	match (a:_facebook {id:'100005519621836'}) - [r:friend] -> (b:_facebook {id:'219500287'}) return r
	
5.search for language user

	match (a:_facebook:#{lan}) return a

6.find a person's group
	
	match (a:_facebook {id:'219500287'}) - [r:join] -> (b:_a_group) return r

7.find all groups 

	match (a:_a_group) return a

8.show all user-join group

	match (a:_facebook) - [r:join] -> (b:_a_group) return r

10.find common join group 

	match (a1:_facebook) - [r1:join] -> (b:_a_group) <- [r2:join] - (a2:_facebook) return r1, r2

11. find languages used

	match (a:_facebook) return distinct(labels(a))
'''

def sendq(sentence):
	req = urllib2.Request(root)
	data = {}
	data["statements"] = []
 	data["statements"].append({
 		"statement":sentence
 		});
 	req.add_header("Content-Type", "application/json")
	req.add_header("Accept","application/json; charset=UTF-8")
	res = urllib2.urlopen(req, json.dumps(data))
	print res.read()

def add_person_node(id,lang,name):
	lang = lang.replace('-','_')
	str = "create (e:_facebook:%s {id:'%s', name:'%s'})" % (lang, id, name)	
	sendq(str)

def add_friend_edge(lid, rid, llabel, rlabel):
	llabel = llabel.replace('-','_')
	rlabel = rlabel.replace('-','_')
	str = "match (l:_facebook {id:'%s'}) match (r:_facebook {id:'%s'}) create (l) - [:friend] -> (r)" % (lid,rid)
	sendq(str)

def add_group_node(id, name, description, belong):
	name = "".join(name.split())
	#description = "".join(description.split())
	str = "create (e:_a_group:%s {id:'%s', name:'%s'} )" % (belong, id, name)
	sendq(str)

def add_group_edge(member_id, group_id):
	str  = "match (l:_facebook {id:'%s'}) match (r:_a_group {id:'%s'}) create (l) - [:join] -> (r)" % (member_id, group_id)
	sendq(str)

def clear():
	str = "match (a) optional match (a)-[r]-() delete a, r"
	sendq(str)


if __name__ == "__main__":
	
	if len(sys.argv) != 2:
		print "usage python sendtoneo4j.py folder"
		sys.exit(0)
	data_root += sys.argv[1] + "/"
	clear()
	
	data = json.loads(open(data_root + "neo4j_data.json","r").read())
	
	#load person
	if "node" in data:
		for item in data["node"]:
			add_person_node(item[0], item[1], item[2])
			node_st[item[0]] = item[1]
	
	#load friend_link
	if "link" in data:
		for edge in data["link"]:
			add_friend_edge(edge["source"], edge["target"], node_st[edge["source"]], node_st[edge["target"]])

	#load group_link_node
	if "group" in data:
		print "fuck"
		for group in data["group"]:
			#name description belong member id
			des = "und"
			if "description" in group:
				des = group["description"]
			add_group_node(group["id"], group["name"], des, group["belong"])
			for member_id in group["member"]:
				add_group_edge(member_id, group["id"])


